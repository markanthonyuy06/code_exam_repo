<?php
include('config.php');

$json['error'] = array();
$allowedExts = array("gif", "jpeg", "jpg", "png","doc","docx","txt","xls","xlsx");
$extension = @end(explode(".", $_FILES["file"]["name"]));
$upload_dir = "uploaded_files/";


if($_POST['id']){
    if ($result = $db->query("SELECT * FROM files_uploaded where id=".$_POST['id'])) {
      $row = $result -> fetch_array(MYSQLI_ASSOC);
      unlink($row['dir']);
    }
}



if ( in_array(strtolower($extension), $allowedExts)){
    if ($_FILES["file"]["error"] > 0){
        $json['error'][] .= "Return Code: " . $_FILES["file"]["error"]; 
    }
    else{
        if (file_exists($upload_dir . $_FILES["file"]["name"])){

            $json['error'][] .= $_FILES["file"]["name"] . " already exists.";
          }
        else{
          move_uploaded_file($_FILES["file"]["tmp_name"],"uploaded_files/" . $_FILES["file"]["name"]);
          $json['data']['file_ext'] = strtolower($extension);
          $json['data']['dir'] = $upload_dir . $_FILES["file"]["name"];
          $json['data']['filename'] = $_FILES["file"]["name"];
          $json['data']['title'] = $_POST['title'];
          $json['data']['date_added'] = date('Y-m-d');
          
          
            if($_POST['id']){
              $query = "UPDATE files_uploaded SET `title`='".$json['data']['title']."', `filename`='". $json['data']['filename']."' , `dir` = '".$json['data']['dir']."',`file_ext` = '".$json['data']['file_ext']."',`date_added` = '".$json['data']['date_added']."' WHERE id =".$_POST['id'];
            }
            else{
              $query = "INSERT INTO files_uploaded (`title`, `filename`, `dir`,`file_ext`,`date_added`)
                   VALUES ('".$json['data']['title']."','". $json['data']['filename']."','".$json['data']['dir']."','".$json['data']['file_ext']."','".$json['data']['date_added']."')";
                   
          
            }
            if ($db->query($query) === TRUE) {
                if($_POST['id']){
                  $json['data']['id']= $_POST['id'];
                }
                else{
                  $json['data']['id']= $db->insert_id;
                }
              // echo "New record created successfully";
            } else {
              $json['error'][] = $query . "<br>" . $db->error;
            }
          }
      }
  }
else
  {
    $json['error'][] .= "Invalid file";
  }

  if(count($json['error']) <=0){
    unset($json['error']);
  }
  echo json_encode($json);
?>