<?php 
include('config.php');
?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Coding Exam. Modal form CRUD with File Upload</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
  <style>
    label, input { display:block; }
    input.text { margin-bottom:12px; width:95%; padding: .4em; }
    fieldset { padding:0; border:0; margin-top:25px; }
    h1 { font-size: 1.2em; margin: .6em 0; }
    div#file-contain { width: 350px; margin: 20px 0; }
    div#file-contain table { margin: 1em 0; border-collapse: collapse; width: 100%; }
    div#file-contain table td, div#file-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; }
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
  </style>
  <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
  <script>
  $( function() {
    var dialog, form,
 
      // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
      emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
      file = $( "#file" ),
      title = $( "#title" ),
      allFields = $( [] ).add( file ).add( title ),
      tips = $( ".validateTips" );
 

    function submitForm() {
      
      var valid = true;
      allFields.removeClass( "ui-state-error" );
 
      valid = valid && checkLength( title, "title", 3, 16 );
      valid = valid && checkLength( file, "file", 5, 80 );
    //   valid = valid && checkLength( password, "password", 5, 16 );
 
    //   valid = valid && checkRegexp( title, /^[a-z]([0-9a-z_\s])+$/i, "Title may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
    //   valid = valid && checkRegexp( email, emailRegex, "eg. ui@jquery.com" );
    //   valid = valid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );
 
      if ( valid ) {
        var fd = new FormData(document.getElementById("fileinfo"));
        $.ajax({
            url: "upload_file.php",
            type: "POST",
            data: fd,
            processData: false,  
            contentType: false  
        }).done(function( data ) {
    
            let myData  = JSON.parse(data);
            if (typeof myData.error === 'undefined') {


              let html_container =  "<td>" + myData.data.title + "</td>" +
                                    "<td><img style='width:100%' src='" + myData.data.dir+"'></td>" +
                                    "<td>" + myData.data.filename + "</td>" +
                                    "<td>" + myData.data.date_added + "</td>" +
                                    "<td><button class='edit-file' data-record-id='"+myData.data.id+"'>Edit</button><button class='delete-file' data-record-id='"+myData.data.id+"'>Delete</button></td>"

              if ( $( "#tr_"+myData.data.id ).length ) {
                $(  "#tr_"+myData.data.id ).html(html_container
                                        
                                        );
              }
              else{
                $( "#file tbody" ).append( "<tr id='tr_"+myData.data.id+"'>" +
                html_container+
                                        "</tr>" );

              }

                dialog.dialog( "close" );
            }
            else{

              updateTips(myData.error);
            }
            
        
        });
        // return false;
        
      }
      else{
          return valid;
      }
  }  
    function updateTips( t ) {
      tips
        .text( t )
        .addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 1500 );
      }, 500 );
    }
 
    function checkLength( o, n, min, max ) {
      if ( o.val().length > max || o.val().length < min ) {
        o.addClass( "ui-state-error" );
        updateTips( "Length of " + n + " must be between " +
          min + " and " + max + "." );
        return false;
      } else {
        return true;
      }
    }
 
    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
 
 
    dialog = $( "#dialog-form" ).dialog({
      autoOpen: false,
      height: 400,
      width: 350,
      modal: true,
      buttons: {
        "Submit": submitForm,
        Cancel: function() {
          dialog.dialog( "close" );
        }
      },
      close: function() {
        form[ 0 ].reset();
        allFields.removeClass( "ui-state-error" );
      }
    });
 
    form = dialog.find( "form" ).on( "submit", function( event ) {
      event.preventDefault();
      processFile();
    });
 
    function create(){
      form[ 0 ].reset();
      dialog.dialog({ title: "Create New File" }).dialog('open');
    }

    $( ".create-file" ).on( "click", function() {
      form[ 0 ].reset();
      dialog.dialog({ title: "Create New File" }).dialog('open');
    });
    $( "#file" ).on( "click",'.edit-file', function() {

      let id = $(this).attr("data-record-id");
      // console.log(id)
      $.getJSON('retrieve_data.php', {id:id}, function (data, textStatus, jqXHR){
        dialog.dialog({ title: "Edit File" }).dialog('open');
        let myData  = data;
         $('#id').val(myData.data.id);
         $('#title').val(myData.data.title);
         $('#filename').val(myData.data.dir);
      });
    });

    $( "#file" ).on( "click",'.delete-file', function() {

       let id = $(this).attr("data-record-id");
       if(confirm('Delete this data?')){
        $.post('delete_data.php', {id:id}, function (response){
          // console.log(response)
          let myData  =JSON.parse(response);
         $('#tr_'+myData.data.id).remove();

        });

       }

    })


  } );
  </script>
</head>
<body>
 
<div id="dialog-form">
  <p class="validateTips">All form fields are required.</p>
 
  <form method="post" id="fileinfo" name="fileinfo" onsubmit="return submitForm();">
    <fieldset>
    <input type="hidden" name="id" id="id">
      <label for="title">Title</label>
      <input type="input" name="title" id="title">
      <label for="file">File</label>
      <input type="file" name="file" id="file">
      <!-- Allow form submission with keyboard without duplicating the dialog button -->
      <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
    </fieldset>
  </form>
</div>
 
 
<div id="file-contain">
  <h1>Existing file:</h1>
  <table id="file" style="width:100%">
    <thead>
      <tr class="ui-widget-header ">
        <th width="20%" style="width:20%">Title</th>
        <th width="20%" style="width:20%">Thumbnail</th>
        <th width="20%" style="width:20%">Filename</th>
        <th width="20%" style="width:20%">Date Added</th>
        <th width="20%" style="width:20%">Action</th>
      </tr>
    </thead>
    <tbody>
        <?php 
        if ($result = $db->query("SELECT * FROM files_uploaded")) {

            // Associative array
           
            while ($row = $result -> fetch_array(MYSQLI_ASSOC)) {
                echo "<tr id='tr_".$row['id']."'>"
                        ."<td>".$row['title']."</td>"
                        ."<td><img style='width:100%;' src='".$row['dir']."'></td>"
                        ."<td>".$row['filename']."</td>"
                        ."<td>".$row['date_added']."</td>"
                        ."<td><button class='edit-file' data-record-id='".$row['id']."'>Edit</button><button class='delete-file' data-record-id='".$row['id']."'>Delete</button></td>"
                ."</tr>";
                // echo "Returned rows are: " . $result -> num_rows;
            // Free result set
            }
            $result -> free_result();
          }
        ?>
      <tr>
       
      </tr>
    </tbody>
  </table>
</div>
<button   class='create-file'>Create New File</button>
 
 
</body>
</html>